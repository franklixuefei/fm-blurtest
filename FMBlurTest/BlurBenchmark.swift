//
//  BlurBenchmark.swift
//  FMBlurTest
//
//  Created by Tomislav Grbin on 30/09/14.
//  Copyright (c) 2014 Tomislav Grbin. All rights reserved.
//

import Foundation
import GPUImage

class BlurBenchmark : NSObject {
    let kMaxBlurRadius = 50.0
    let kBlurDelta = 0.9
    let kSameBlurRepeat = 5
    
    var originalImage : UIImage
    var gpuBlurFilter : GPUImageGaussianBlurFilter
    var gpuBluriOSFilter : GPUImageiOSBlurFilter
    var ciContext : CIContext
    var ciImage : CIImage
    var ciFilter : CIFilter
    
    override init() {
        originalImage = UIImage(named: "screen")!
        gpuBlurFilter = GPUImageGaussianBlurFilter()
        gpuBluriOSFilter = GPUImageiOSBlurFilter()
        
        ciContext = CIContext(options: nil)
        ciImage = CIImage(image: originalImage)
        ciFilter = CIFilter(name: "CIGaussianBlur")
        ciFilter.setValue(ciImage, forKey: kCIInputImageKey)
        
        super.init()
    }
    
    func run() {
        NSLog("Core Image blur")
        run(coreImageBlur)
        
        NSLog("GPUImage blur")
        run(gpuImageBlur)
        
        NSLog("GPUImage iOS blur")
        run(gpuImageiOSBlur)
        
        NSLog("Apple's ImageEffects category blur")
        run(imageEffectsBlur)
    }
    
    func run(blurFunc : (Double) -> (UIImage)) {
        
        // first calls sometimes take longer, we'll ignore them
        blurFunc(2)
        
        for var blurRadius = 0.0; blurRadius < kMaxBlurRadius; blurRadius += kBlurDelta {
            var timeSum = 0.0
            
            let localConstant = kSameBlurRepeat
            autoreleasepool {
                for _ in 1...localConstant {
                    var stamp = NSDate()
                    blurFunc(blurRadius)
                    timeSum += -stamp.timeIntervalSinceNow
                }
                
                var time = timeSum / Double(localConstant)
                var fps = 1 / time
                NSLog("radius: \(blurRadius), time: \(time * 1000) ms, fps: \(fps)")
            }
        }
    }
    
    func coreImageBlur(radius: Double) -> UIImage {
        ciFilter.setValue(radius, forKey: "inputRadius")
        
        var cgImage = ciContext.createCGImage(ciFilter.outputImage, fromRect: ciImage.extent())
        
        return UIImage(CGImage: cgImage)!
    }
    
    func gpuImageBlur(radius: Double) -> UIImage {
        gpuBlurFilter.blurRadiusInPixels = CGFloat(radius)
        return gpuBlurFilter.imageByFilteringImage(originalImage)
    }
    
    func gpuImageiOSBlur(radius: Double) -> UIImage {
        gpuBluriOSFilter.blurRadiusInPixels = CGFloat(radius)
        return gpuBluriOSFilter.imageByFilteringImage(originalImage)
    }
    
    func imageEffectsBlur(radius: Double) -> UIImage {
        return originalImage.applyBlurWithRadius(CGFloat(radius), tintColor: nil, saturationDeltaFactor: 1.0, maskImage: nil)
    }
}
